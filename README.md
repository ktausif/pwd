Saving credentials online can be little dangerous even if the system is highly secure. A new way of saving credentials in a personal system can be helpful. Without this system not even the account holder can see the encrypted credentials.
![image](/uploads/cb35e3099da3f682e6b1bcbf3e9dd0b6/image.png)
![image](/uploads/bf6fa438d1205d5e89577c7e1bb3177d/image.png)
![image](/uploads/df61ca5c4b36838a57bad3eed253fd8c/image.png)
